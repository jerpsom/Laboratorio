/*Autor: Jerpsom Andree Flores Quispe
*************************************
UNIVERSIDAD NACIONAL DE SAN AGUSTIN
ESCUELA PROFESIONAL DE INGENIERIA EN TELECOMUNICACIONES
*******************************************************
CURSO : COMPUTACION 1
FECHA : 12 DE OCTUBRE 2017
Enunciado:
Ejemplo de salida estandar con setfill y setw
*/
#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    //Variables
    double a1 = 205.549;
    double a2 = 4.18;

    //Proceso
    //Salida Normal
    cout<< setw(60) <<"Este programa visualiza salida estandar mejorada";
    cout <<"a1 -->" << a1 << endl;
    cout <<"a2 -->" << a2 << endl;
    cout << endl;

    //Salida utilizando fixed relleno de ceros
    cout << fixed << "a1 -->" << a1 << endl;
    cout << "a2 -->" << a2 << endl;
    cout << endl;

    //Salida Precision 1 Decimal
    cout << "a1 -->" << setprecision(1)<< a1 << endl;
    cout << "a2 -->" << a2 << endl;
    cout << endl;

    //Salida con relleno de Simbolo
    cout << "a1 -->" << setw(10) << setfill('.') << a1 << endl;
    cout << "a2 -->" << a2 << endl;
    cout << "a1 -->" << setw(12) << a1 <<endl;
    cout << endl;

    return 0;
}

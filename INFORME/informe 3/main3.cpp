/* AUTOR JERPSOM ANDREE FLORES QUISPE
*************************************
UNIVERSIDAD NACIONAL DE SAN AGUSTIN
ESCUELA PROFESIONAL DE INGENIERIA EN TELECOMUNICACIONES
*******************************************************
CURSO: COMPUTACION 1
FECHA : 12 DE OCTUBRE 2017
ENUNCIADO:
Hacer un programa que permita el ingreso del radio de un círculo por teclado y calcule:
Longitud de la Circunferencia.
Área del Círculo.
Volumen correspondiente a ese radio.
*/
#include <iostream>
#include <iomanip>
#include <math.h>
using namespace std;

int main()
{
    //Constantes
    const double PI = 3.1416;

    //Varibales
    int r, h;
    double l, a, v;

    //entrada
    cout << "Ingrese el radio del circulo" << endl; cin >> r;

    //Proceso

     l = 2*r*PI;
     a = PI*pow(r,2);
     v = (4*PI*pow(r,3) ) / 3;

    //Salida

    cout << "La longitud de la circunferencia es: "<< setprecision(3)<< l << endl;
    cout << "El area del circulo es: " << setprecision(3)<< a << endl;
    cout << "El volumen de la esfera es : " <<  setprecision(3)<< v << endl;

    return 0;
}

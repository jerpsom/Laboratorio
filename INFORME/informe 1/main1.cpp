/* AUTOR JERPSOM ANDREE FLORES QUISPE
*************************************
UNIVERSIDAD NACIONAL DE SAN AGUSTIN
ESCUELA PROFESIONAL DE INGENIERIA EN TELECOMUNICACIONES
*******************************************************
CURSO: COMPUTACION 1
FECHA : 12 DE OCTUBRE 2017
ENUNCIADO:
HACER UN PROGRAMA QUE CONVIERTA UN NUMERO DE SEGUNDOS EN HORAS, MINUTOS Y SEGUNDOS.
*/
#include <iostream>

using namespace std;

int main()
{
    //Constantes
    const int HORA = 3600;
    const int MINUTO = 60;

    //Variables
    int t,h,m,s;

    //Entrada
    cout << "Tiempo en segundos: "; cin>> t;

    //Proceso
    h = t / HORA;
    t = t % HORA;
    m = t / MINUTO;
    s = t % MINUTO;

    //Salida
    cout <<"\n";
    cout <<"Hora:" <<h<<"\n";
    cout <<"Minuto" <<m<<"\n";
    cout <<"Segundo" <<s<<"\n";
    return 0;
}

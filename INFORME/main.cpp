/* AUTOR: RENZO BOLIVAR VALDIVIA
*****************************************************
UNIVERSIDAD NACIONAL DE SAN AGUSTIN
ESCUELA PROFESIONAL DE INGENIERIA TELECOMUNICACIONES
*****************************************************
CURSO : COMPUTACION 1
FECHA: 13 OCTUBRE 2017

ENUNCIADO:
SE TIENE UNA CANTIDAD DEN MILIGRAMOS EXPRESARLO EN KILOGRAMOS GRAMOS Y MILIGRAMOS
*/
#include<iostream>

using namespace std;

int main()
{
    //constantes
    const int A=1000;
    
    //variables
    int k,g,mg,rmg;
    
    //entrada
    cout<< "BIENVENIDOS\n\n";
    cout<< "Ingrese los DATOS: "; cin>>mg;
    cout<< "\n";
    
    //proceso
    k=mg/A;
    mg=mg%A;
    g=mg/A;
    mg=mg%A;
    rmg=mg;

    //salida
    cout<<"Kilos: " << k <<"\n";
    cout<<"Gramos: " << g <<"\n";
    cout<<"Miligramos: " << rmg <<"\n\n";
    cout<<"\n\n";
    return 0;
}
